from django.conf import settings
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from sms import tasks


kannel_delivery_status = {
    0: u'Unprocessed',
    1: u'Success',
    2: u'Failure',
    4: u'Buffered',
    8: u'Submitted',
    16: u'Rejected',
}


class SmsManager(models.Manager):
    def get_for(self, obj):
        qs = super(SmsManager, self).get_query_set()
        if obj is None:
            qs = qs.filter(obj_id=None, obj_content_type=None)
        else:
            content_type = ContentType.objects.get_for_model(obj)
            qs = qs.filter(obj_id=obj.pk, obj_content_type=content_type)

        return qs


class Sms(models.Model):
    sender = models.CharField(_(u"Sender"), max_length=20)
    phone = models.CharField(_(u"Phone number"), max_length=15)
    text = models.TextField(_(u"Text"))
    time = models.DateTimeField(_(u"Timestamp"), auto_now_add=True)
    sent = models.BooleanField(_(u"Sent to gateway"), default=False)
    delivery_status = models.IntegerField(_(u"Delivery status"), default=0,
                                          choices=kannel_delivery_status.items())

    obj_content_type = models.ForeignKey(ContentType, null=True)
    obj_id = models.PositiveIntegerField(null=True, db_index=True)
    obj = generic.GenericForeignKey('obj_content_type', 'obj_id')
    type = models.IntegerField(null=True)

    STATUS_SUCCESS = 1
    STATUS_FAILURE = 2
    STATUS_SUBMITTED = 8
    STATUS_REJECTED = 16

    def __unicode__(self):
        status = self.get_delivery_status_display()
        return u"{} {} {}".format(self.phone, status, self.obj)

    def save(self, *args, **kwargs):
        if self.id is None and not self.sender:
            self.sender = settings.KANNEL_SMS_SENDER

        super(Sms, self).save(*args, **kwargs)

    objects = SmsManager()


@receiver(post_save, sender=Sms)
def send_sms_on_save(sender, instance, created, **kwargs):
    if created:
        kwargs = {}
        if hasattr(settings, 'KANNEL_CELERY_QUEUE'):
            kwargs['queue'] = settings.KANNEL_CELERY_QUEUE
        tasks.send_sms.apply_async((instance.pk,), **kwargs)
