from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url('^(?P<sms_id>\d+)/(?P<status>\d+)$', 'sms.views.delivery_report', name='delivery-report')
)
