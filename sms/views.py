import logging

from django.http import HttpResponse

from sms.models import Sms
from sms.signals import sms_status_updated


logger = logging.getLogger(__name__)


def delivery_report(request, sms_id, status):
    try:
        sms = Sms.objects.get(pk=sms_id)
        delivery_status = int(status)
    except (Sms.DoesNotExist, TypeError, ValueError):
        return HttpResponse('', status=404, content_type='text/plain')

    old_status = sms.delivery_status
    sms.delivery_status = delivery_status
    sms.save()

    if old_status != sms.delivery_status:
        res = sms_status_updated.send_robust(sender=sms, old_status=old_status)
        for handler, retval in res:
            if isinstance(retval, Exception):
                logger.error(u"Sms status update signal error: %s", repr(retval))

    return HttpResponse('', content_type='text/plain')
