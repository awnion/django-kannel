from django.contrib import admin
from sms.models import Sms


class SmsAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'delivery_status', 'time']
    list_filter = ['delivery_status', 'time', 'sent']
    search_fields = ['phone']

admin.site.register(Sms, SmsAdmin)
