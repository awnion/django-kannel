from django.dispatch import Signal


sms_status_updated = Signal(providing_args=["old_status"])
