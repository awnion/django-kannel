#!/usr/bin/env python
from setuptools import setup, find_packages


version = '0.1.0'

setup(
    name='django-kannel',
    version=version,
    description="Django interface for Kannel",
    long_description=open("README.md").read(),
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Django",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    keywords='',
    author='adonweb',
    author_email='dev@fitshop.me',
    url='',
    license='BSD',
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'setuptools',
        # -*- Extra requirements: -*-
    ],
    entry_points="""
    # -*- Entry points: -*-
    """,
)
